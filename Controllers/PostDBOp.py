import sys
from Controllers.DBOp import DBOp
from DB.PostDB import PostDB
from Models.User import User

class PostDBOp:

    @classmethod
    def getPostById(self, id:int, userId:int):
        if userId is None or userId == 0:
            raise Exception('You need to login first!')
        conn = DBOp.getConnection()
        try:
            post = PostDB.getPostById(conn, id)

            DBOp.closeConnection(conn)
            return post
        except:
            print("::PostDBOp getPostById error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise

    @classmethod
    def getAllPosts(self, userId:int):
        if userId is None or userId == 0:
            raise Exception('You need to login first!')
        conn = DBOp.getConnection()
        try:
            postList: list = PostDB.getAllPosts(conn)
            for post in postList:
                isLiked:bool = PostDB.isPostLiked(conn, post.getPostId(), userId)
                post.setIsLiked(isLiked)

            DBOp.closeConnection(conn)
            return postList
        except:
            print("::DBOp getAllPosts error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise

    @classmethod
    def addNewPost(self, message:str, mediaArr:str, userId:int):
        if userId is None or userId == 0:
            raise Exception('You need to login first!')
        #loggedinUser:User = DBOp.getLoggedinUser()
        conn = DBOp.getConnection()
        try:
            PostDB.addNewPost(conn, userId, message, mediaArr)

            DBOp.closeConnection(conn)
            return True
        except:
            print("::DBOp addNewPost error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise

    @classmethod
    def editPost(self, message: str, postId:int, userId:int):
        if userId is None or userId == 0:
            raise Exception('You need to login first!')
        #loggedinUser: User = DBOp.getLoggedinUser()
        conn = DBOp.getConnection()
        try:
            PostDB.editPost(conn, postId, userId, message)

            DBOp.closeConnection(conn)
            return True
        except:
            print("::DBOp editPost error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise

    @classmethod
    def likePost(self, postId:int, userId:int):
        conn = DBOp.getConnection()
        try:
            if userId is None or userId == 0:
                raise Exception('You need to login first!')
            #loggedinUser: User = DBOp.getLoggedinUser()
            PostDB.likePost(conn, postId, userId)

            DBOp.closeConnection(conn)
            return True
        except:
            print("::DBOp likePost error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise

    @classmethod
    def unlikePost(self, postId: int, userId:int):
        conn = DBOp.getConnection()
        try:
            if userId is None or userId == 0:
                raise Exception('You need to login first!')
            #loggedinUser: User = DBOp.getLoggedinUser()
            PostDB.unlikePost(conn, postId, userId)

            DBOp.closeConnection(conn)
            return True
        except:
            print("::DBOp unlikePost error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise
