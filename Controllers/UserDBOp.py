import sys
from Controllers.DBOp import DBOp
from DB.UserDB import UserDB
from Utils.SidGenerator import SidGenerator


class UserDBOp:

    @classmethod
    def getUserById(self, id:int):
        if DBOp.getLoggedinUser() is None:
            raise Exception('You need to login first!')
        conn = DBOp.getConnection()
        try:
            user = UserDB.getUserById(conn, id)

            DBOp.closeConnection(conn)
            return user
        except:
            print("::DBOp getUserById error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise

    @classmethod
    def getUserByEmailAndPwd(self, email:str, pwd:str):
        if DBOp.getLoggedinUser() is None:
            raise Exception('You need to login first!')
        conn = DBOp.getConnection()
        try:
            user = UserDB.getUserByEmailAndPwd(conn, email, pwd)

            DBOp.closeConnection(conn)
            return user
        except:
            print("::DBOp getUserByEmailAndPwd error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise

    @classmethod
    def getAllUsers(self):
        conn = DBOp.getConnection()
        try:
            userList:list = UserDB.getAllUsers(conn)

            DBOp.closeConnection(conn)
            return userList
        except:
            print("::DBOp getAllUsers error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise

    @classmethod
    def login(self, email: str, pwd: str, uname:str=None):
        conn = DBOp.getConnection()
        try:
            result_dict: object = {}
            user = None
            if email is not None:
                user = UserDB.getUserByEmailAndPwd(conn, email, pwd)
            elif uname is not None:
                user = UserDB.getUserByUnameAndPwd(conn, uname, pwd)

            if user is not None:
                DBOp.setLoggedinUser(user)
                sid = SidGenerator.generateSid()
                UserDB.insertSid(conn, sid, user.email)
                result_dict['user'] = user
                result_dict['sid'] = sid
            else:
                result_dict['user'] = None
                result_dict['sid'] = None

            DBOp.closeConnection(conn)
            return result_dict
        except:
            print("::DBOp login error::", sys.exc_info()[1])
            DBOp.closeConnection(conn)
            raise Exception('wrong email or password!')

    @classmethod
    def getUserBySid(self, sid: str):
        conn = DBOp.getConnection()
        try:
            user = UserDB.getUserBySid(conn, sid)

            DBOp.closeConnection(conn)
            DBOp.setLoggedinUser(user)
            return user
        except:
            print("::DBOp getUserBySid error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise

    @classmethod
    def logout(self, email: str, sid:str):

        conn = DBOp.getConnection()
        try:
            UserDB.logout(conn, email, sid)
            DBOp.setLoggedinUser(None)
            DBOp.closeConnection(conn)
            return True
        except:
            print("::DBOp logout error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise

    @classmethod
    def insertUser(self, email: str, pwd:str, uname:str, fname:str, type:int, imgUrl:str):
        conn = DBOp.getConnection()
        try:
            UserDB.insertUser(conn, email, pwd, uname, fname, type, imgUrl)

            DBOp.closeConnection(conn)

            return True
        except:
            print("::DBOp insertUser error::", sys.exc_info()[0])
            DBOp.closeConnection(conn)
            raise