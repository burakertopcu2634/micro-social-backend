import pymysql
from Models.User import User

class DBOp:
    loggedinUser:User = None

    @classmethod
    def getConnection(self):
        conn = pymysql.connect(host = 'localhost', user = 'root', passwd = '12345678', db = 'micro-social', charset='utf8mb4')
        return conn

    @classmethod
    def closeConnection(self, conn:pymysql.Connection):
        conn.close()

    @classmethod
    def setLoggedinUser(self, user:User):
        self.loggedinUser = user

    @classmethod
    def getLoggedinUser(self):
        return self.loggedinUser