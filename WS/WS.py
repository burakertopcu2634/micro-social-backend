import logging

import werkzeug
from flask import Flask, jsonify, request

from Controllers.DBOp import DBOp
from Controllers.PostDBOp import PostDBOp
from Controllers.UserDBOp import UserDBOp
from flask_cors import CORS
from flask_httpauth import HTTPTokenAuth
from waitress import serve

#logging.basicConfig(filename='micro-social-error.log', level=logging.DEBUG)

app = Flask(__name__)
CORS(app)
auth = HTTPTokenAuth(scheme='Bearer')

@auth.verify_token
def verify_token(token:str):
   if token is None or len(token.strip()) == 0:
      raise Exception("You need to login first!")

   user = UserDBOp.getUserBySid(token)
   DBOp.setLoggedinUser(user)
   return user

@app.errorhandler(Exception)
def handle_error(e):
    return jsonify(str(e))

@app.route('/login', methods=['POST'])
def login():
   """
   @api {post} /login login

   @apiName login
   @apiGroup User

   @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.

   @apiParam {String} email Users' unique email.
   @apiParam {String} uname Users' unique username.
   @apiParam {String} pwd Users' password.

   @apiSuccess {Number} userId id of the User.
   @apiSuccess {String} email  email of the User.
   @apiSuccess {String} username  username of the User.
   @apiSuccess {String} fullName  full name of the User.
   @apiSuccess {Number} type  type of the User, 1 corresponds to admin rest are standard users.
   @apiSuccess {String} [imageUrl]  avatar url of the User.
   @apiSuccess {Number} createdTime  the date that User is created in milliseconds.
   @apiSuccess {Number} [modifiedTime]  the date that User is modified in milliseconds.
   """

   request_data = request.get_json()
   email = request_data['email']
   uname = request_data['uname']
   pwd = request_data['pwd']
   result_dict = UserDBOp.login(email, pwd, uname)
   user = result_dict['user'] if result_dict['user'] is not None else None
   sid = result_dict['sid'] if result_dict['sid'] is not None else None
   result = {'user': user.jsonString(), 'sid': sid}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson

@app.route('/getLoggedinUser', methods=['POST'])
def getLoggedinUser():
   """
      @api {post} /getLoggedinUser getLoggedinUser

      @apiName getLoggedinUser
      @apiGroup User

      @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.

      @apiParam {String} sid User's session token.

      @apiSuccess {Number} userId id of the User.
      @apiSuccess {String} email  email of the User.
      @apiSuccess {String} username  username of the User.
      @apiSuccess {String} fullName  full name of the User.
      @apiSuccess {Number} type  type of the User, 1 corresponds to admin rest are standard users.
      @apiSuccess {String} [imageUrl]  avatar url of the User.
      @apiSuccess {Number} createdTime  the date that User is created in milliseconds.
      @apiSuccess {Number} [modifiedTime]  the date that User is modified in milliseconds.
   """
   request_data = request.get_json()
   sid = request_data['sid']
   user = UserDBOp.getUserBySid(sid)
   result = {'user': user.jsonString()}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson

@app.route('/logout', methods=['POST'])
def logout():
   """
         @api {post} /logout logout

         @apiName logout
         @apiGroup User

         @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.

         @apiParam {String} email User's email address.
         @apiParam {String} sid User's session token.

         @apiSuccess {Boolean} success status of logout.
   """
   request_data = request.get_json()
   email = request_data['email']
   sid = request_data['sid']
   success = UserDBOp.logout(email, sid)
   result = {'success': success}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson

@app.route('/getUserById', methods=['POST'])
def getUserById():
   """
         @api {post} /getUserById getUserById

         @apiName getUserById
         @apiGroup User

         @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.

         @apiParam {Number} userId User's id.

         @apiSuccess {Number} userId id of the User.
         @apiSuccess {String} email  email of the User.
         @apiSuccess {String} username  username of the User.
         @apiSuccess {String} fullName  full name of the User.
         @apiSuccess {Number} type  type of the User, 1 corresponds to admin rest are standard users.
         @apiSuccess {String} [imageUrl]  avatar url of the User.
         @apiSuccess {Number} createdTime  the date that User is created in milliseconds.
         @apiSuccess {Number} [modifiedTime]  the date that User is modified in milliseconds.
   """
   request_data = request.get_json()
   userId = request_data['userId']
   user = UserDBOp.getUserById(userId)
   result = {'user': user.jsonString()}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson


@app.route('/getUserByEmailAndPwd', methods=['POST'])
def getUserByEmailAndPwd():
   """
         @api {post} /getUserByEmailAndPwd getUserByEmailAndPwd

         @apiName getUserByEmailAndPwd
         @apiGroup User

         @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.

         @apiParam {String} email User's email address.
         @apiParam {String} pwd User's password as plain text.

         @apiSuccess {Number} userId id of the User.
         @apiSuccess {String} email  email of the User.
         @apiSuccess {String} username  username of the User.
         @apiSuccess {String} fullName  full name of the User.
         @apiSuccess {Number} type  type of the User, 1 corresponds to admin rest are standard users.
         @apiSuccess {String} [imageUrl]  avatar url of the User.
         @apiSuccess {Number} createdTime  the date that User is created in milliseconds.
         @apiSuccess {Number} [modifiedTime]  the date that User is modified in milliseconds.
   """
   request_data = request.get_json()
   email = request_data['email']
   pwd = request_data['pwd']
   user = UserDBOp.getUserByEmailAndPwd(email, pwd)
   result = {'user': user.jsonString()}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson

@app.route('/getAllUsers', methods=['POST'])
def getAllUsers():
   """
         @api {post} /getAllUsers getAllUsers

         @apiName getAllUsers
         @apiGroup User

         @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.

         @apiSuccess {Object[]} userList  List of users.
         @apiSuccess {Number} userList.userId id of the User.
         @apiSuccess {String} userList.email  email of the User.
         @apiSuccess {String} userList.username  username of the User.
         @apiSuccess {String} userList.fullName  full name of the User.
         @apiSuccess {Number} userList.type  type of the User, 1 corresponds to admin rest are standard users.
         @apiSuccess {String} [userList.imageUrl]  avatar url of the User.
         @apiSuccess {Number} userList.createdTime  the date that User is created in milliseconds.
         @apiSuccess {Number} [userList.modifiedTime]  the date that User is modified in milliseconds.
   """
   userList = UserDBOp.getAllUsers()
   userListJson = jsonify(userList=[user.jsonString() for user in userList])
   userListJson.headers.add('Access-Control-Allow-Origin', '*')
   return userListJson

@app.route('/getAllPosts', methods=['POST'])
def getAllPosts():
   """
         @api {post} /getAllPosts getAllPosts

         @apiName getAllPosts
         @apiGroup Post

         @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.
         @apiHeader (Header) {Bearer} Authorization Authorization token.

         @apiSuccess {Object[]} postList  List of posts.
         @apiSuccess {Number} postList.postId id of the Post.
         @apiSuccess {Number} postList.userId  user id of the Post.
         @apiSuccess {Object} postList.user  user who created the Post.
         @apiSuccess {Number} postList.user.userId id of the User.
         @apiSuccess {String} postList.user.email  email of the User.
         @apiSuccess {String} postList.user.username  username of the User.
         @apiSuccess {String} postList.user.fullName  full name of the User.
         @apiSuccess {Number} postList.user.type  type of the User, 1 corresponds to admin rest are standard users.
         @apiSuccess {String} [postList.user.imageUrl]  avatar url of the User.
         @apiSuccess {Number} postList.user.createdTime  the date that User is created in milliseconds.
         @apiSuccess {Number} [postList.user.modifiedTime]  the date that User is modified in milliseconds.
         @apiSuccess {String} postList.message  content message of the Post.
         @apiSuccess {String} [postList.mediaArray]  array-like string of images/videos in the Post.
         @apiSuccess {Number} postList.createdTime  the date that User is created in milliseconds.
         @apiSuccess {Number} [postList.modifiedTime]  the date that User is modified in milliseconds.
         @apiSuccess {Boolean} postList.isLiked  True if the Post is liked by logged-in user.
      """
   request_data = request.get_json()
   userId = request_data['userId']
   postList = PostDBOp.getAllPosts(userId)
   postListJson = jsonify(postList=[post.jsonString() for post in postList])
   postListJson.headers.add('Access-Control-Allow-Origin', '*')
   return postListJson

@app.route('/getPostById', methods=['POST'])
def getPostById():
   """
         @api {post} /getPostById getPostById

         @apiName getPostById
         @apiGroup Post

         @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.
         @apiHeader (Header) {Bearer} Authorization Authorization token.

         @apiParam {Nubmer} postId id of the Post.

         @apiSuccess {Number} postId id of the Post.
         @apiSuccess {Number} userId  user id of the Post.
         @apiSuccess {Object} user  user who created the Post.
         @apiSuccess {Number} user.userId id of the User.
         @apiSuccess {String} user.email  email of the User.
         @apiSuccess {String} user.username  username of the User.
         @apiSuccess {String} user.fullName  full name of the User.
         @apiSuccess {Number} user.type  type of the User, 1 corresponds to admin rest are standard users.
         @apiSuccess {String} user.imageUrl]  avatar url of the User.
         @apiSuccess {Number} user.createdTime  the date that User is created in milliseconds.
         @apiSuccess {Number} [user.modifiedTime]  the date that User is modified in milliseconds.
         @apiSuccess {String} message  content message of the Post.
         @apiSuccess {String} [mediaArray]  array-like string of images/videos in the Post.
         @apiSuccess {Number} createdTime  the date that User is created in milliseconds.
         @apiSuccess {Number} [modifiedTime]  the date that User is modified in milliseconds.
         @apiSuccess {Boolean} isLiked  True if the Post is liked by logged-in user.
      """
   request_data = request.get_json()
   postId = request_data['postId']
   post = PostDBOp.getPostById(postId)
   result = {'post': post.jsonString()}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson

@app.route('/addNewPost', methods=['POST'])
def addNewPost():
   """
      @api {post} /addNewPost addNewPost

      @apiName addNewPost
      @apiGroup Post

      @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.
      @apiHeader (Header) {Bearer} Authorization Authorization token.

      @apiParam {String} message Content message of the Post.
      @apiParam {String} [mediaArr] array-like string of photo/video urls.

      @apiSuccess {Boolean} success status of Post addition.
   """
   request_data = request.get_json()
   message = request_data['message']
   mediaArr = request_data['mediaArr']
   userId = request_data['userId']
   isSuccess = PostDBOp.addNewPost(message, mediaArr, userId)
   result = {'success': isSuccess}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson

@app.route('/editPost', methods=['POST'])
def editPost():
   """
      @api {post} /editPost editPost

      @apiName editPost
      @apiGroup Post

      @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.
      @apiHeader (Header) {Bearer} Authorization Authorization token.

      @apiParam {String} message Content message of the Post.
      @apiParam {Number} postId id of the Post.

      @apiSuccess {Boolean} success status of Post addition.
   """
   request_data = request.get_json()
   message = request_data['message']
   postId = request_data['postId']
   userId = request_data['userId']
   isSuccess = PostDBOp.editPost(message, postId, userId)
   result = {'success': isSuccess}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson

@app.route('/likePost', methods=['POST'])
def likePost():
   """
      @api {post} /likePost likePost

      @apiName likePost
      @apiGroup Post

      @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.
      @apiHeader (Header) {Bearer} Authorization Authorization token.

      @apiParam {Number} postId id of the Post.

      @apiSuccess {Boolean} success status of Post addition.
   """
   request_data = request.get_json()
   postId = request_data['postId']
   userId = request_data['userId']
   isSuccess = PostDBOp.likePost(postId, userId)
   result = {'success': isSuccess}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson

@app.route('/unlikePost', methods=['POST'])
def unlikePost():
   """
      @api {post} /unlikePost unlikePost

      @apiName unlikePost
      @apiGroup Post

      @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.
      @apiHeader (Header) {Bearer} Authorization Authorization token.

      @apiParam {Number} postId id of the Post.

      @apiSuccess {Boolean} success status of Post addition.
   """
   request_data = request.get_json()
   postId = request_data['postId']
   userId = request_data['userId']
   isSuccess = PostDBOp.unlikePost(postId, userId)
   result = {'success': isSuccess}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson


@app.route('/insertUser', methods=['POST'])
def insertUser():
   """
         @api {post} /insertUser insertUser

         @apiName insertUser
         @apiGroup User

         @apiHeader (Header) {String} Content-Type=application/json content type of request. It is set to application/json.

         @apiParam {String} email email address.
         @apiParam {String} pwd password.
         @apiParam {String} uname username.
         @apiParam {String} fname full name.
         @apiParam {String} type type of authorization 1 refers to admin, 2 refers to standard user, 3 refers to spectator.
         @apiParam {String} imgUrl avatar's public url.

         @apiSuccess {Boolean} success status of logout.
   """
   request_data = request.get_json()
   email = request_data['email']
   pwd = request_data['pwd']
   uname = request_data['uname']
   fname = request_data['fname']
   type = request_data['type']
   imgUrl = request_data['imgUrl']
   success = UserDBOp.insertUser(email, pwd, uname, fname, type, imgUrl)
   result = {'success': success}
   resultJson = jsonify(result)
   resultJson.headers.add('Access-Control-Allow-Origin', '*')
   return resultJson


if __name__ == '__main__':
   #app.run(host="0.0.0.0")
   app.config['MYSQL_DATABASE_CHARSET'] = 'utf8mb4'
   serve(app, host='0.0.0.0', port=5000)