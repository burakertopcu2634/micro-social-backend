from flask import jsonify
from datetime import datetime

class User:
    def __init__(self, userId:int, email:str, userName:str, fullName:str, type:int, createdTime:datetime, modifiedTime:datetime=None, imageUrl:str=None):
        self.userId = userId
        self.email = email
        self.userName = userName
        self.fullName = fullName
        self.type = type
        self.createdTime = createdTime
        self.modifiedTime = modifiedTime
        self.imageUrl = imageUrl

    @classmethod
    def sqlColumns(self):
        return " users.user_id user_id, users.email user_email, users.username user_username," \
               " users.full_name user_full_name, users.type user_type, users.image_url user_image_url," \
               " users.created_time user_created_time," \
               " users.modified_time user_modified_time "
    @classmethod
    def rs(self, resultSet):
        return User(resultSet['user_id'], resultSet['user_email'], resultSet['user_username'],
                    resultSet['user_full_name'], resultSet['user_type'], resultSet['user_created_time'],
                    resultSet['user_modified_time'], resultSet['user_image_url'])

    def jsonString(self):
        return {
            'userId': self.getUserId(),
            'email': self.getEmail(),
            'username': self.getUserName(),
            'fullName': self.getFullName(),
            'type': self.getType(),
            'imageUrl': self.getImageUrl(),
            'createdTime': int(round(self.getCreatedTime().timestamp() * 1000)),
            'modifiedTime': int(round(self.getModifiedTime().timestamp() * 1000)) if self.getModifiedTime() is not None else None,
        }

    def getUserId(self):
        return self.userId

    def getEmail(self):
        return self.email

    def getUserName(self):
        return self.userName

    def getFullName(self):
        return self.fullName

    def getType(self):
        return self.type

    def getImageUrl(self):
        return self.imageUrl

    def getCreatedTime(self):
        return self.createdTime

    def getModifiedTime(self):
        return self.modifiedTime

    def setUserId(self, userId):
        self.userId = userId

    def setEmail(self, email):
        self.email = email

    def setUserName(self, userName):
        self.userName = userName

    def setFullName(self, fullName):
        self.fullName = fullName

    def setType(self, type):
        self.type = type

    def setImageUrl(self, imageUrl):
        self.imageUrl = imageUrl

    def setCreatedTime(self, createdTime):
        self.createdTime = createdTime

    def setModifiedTime(self, modifiedTime):
        self.modifiedTime = modifiedTime