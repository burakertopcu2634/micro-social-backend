from datetime import datetime

from Models.User import User


class Post:
    def __init__(self, postId:int, userId:int, message:str, mediaArray:str, createdTime:datetime, modifiedTime:datetime=None, isLiked:bool=None, likeCount:int=None):
        self.postId = postId
        self.userId = userId
        self.message = message
        self.mediaArray = mediaArray
        self.createdTime = createdTime
        self.modifiedTime = modifiedTime
        self.user = None
        self.isLiked = isLiked
        self.likeCount = likeCount

    @classmethod
    def sqlColumns(self):
        return " posts.id post_post_id," \
                " posts.user_id post_user_id," \
                " posts.message post_message," \
                " posts.media_array post_media_array," \
                " posts.created_time post_created_time,"\
                " posts.modified_time post_modified_time,"\
                " (SELECT count(*) FROM user_likes WHERE user_likes.post_id = posts.id) post_like_count "
    @classmethod
    def rs(self, resultSet):
        return Post(resultSet['post_post_id'], resultSet['post_user_id'], resultSet['post_message'],
                    resultSet['post_media_array'], resultSet['post_created_time'], resultSet['post_modified_time'],
                    False, resultSet['post_like_count'])

    def jsonString(self):
        return {
            'postId': self.getPostId(),
            'userId': self.getUserId(),
            'user': None if self.getUser() is None else self.getUser().jsonString(),
            'message': self.getMessage(),
            'mediaArray': self.getMediaArray(),
            'createdTime': int(round(self.getCreatedTime().timestamp() * 1000)),
            'modifiedTime': int(round(self.getModifiedTime().timestamp() * 1000)) if self.getModifiedTime() is not None else None,
            'isLiked': self.getIsLiked(),
            'likeCount': self.getLikeCount()
        }

    def getPostId(self):
        return self.postId

    def getUserId(self):
        return self.userId

    def getUser(self) -> User:
        return self.user

    def getMessage(self):
        return self.message

    def getMediaArray(self):
        return self.mediaArray

    def getCreatedTime(self):
        return self.createdTime

    def getModifiedTime(self):
        return self.modifiedTime

    def setPostId(self, postId):
        self.postId = postId

    def setUserId(self, userId):
        self.userId = userId

    def setUser(self, user: User):
        self.user = user

    def setMessage(self, message):
        self.message = message

    def setMediaArray(self, mediaArray):
        self.mediaArray = mediaArray

    def setCreatedTime(self, createdTime):
        self.createdTime = createdTime

    def setModifiedTime(self, modifiedTime):
        self.modifiedTime = modifiedTime

    def getIsLiked(self):
        return self.isLiked

    def setIsLiked(self, isLiked:bool):
        self.isLiked = isLiked

    def getLikeCount(self):
        return self.likeCount

    def setLikeCount(self, likeCount:int):
        self.likeCount = likeCount