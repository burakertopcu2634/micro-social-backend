import sys
import pymysql
import logging
from Models.User import User

class UserDB:
    def __init__(self):
        x=5

    @classmethod
    def getUserById(self, conn:pymysql.Connection, id:int):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "SELECT " + User.sqlColumns() + " FROM users WHERE user_id = %s"

            cursor.execute(query, (id))
            rs = cursor.fetchone()

            user = User.rs(rs)

            return user
        except:
            print("::UserDB getUserById error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def getUserByUnameAndPwd(self, conn: pymysql.Connection, uname: str, pwd:str):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "SELECT " + User.sqlColumns() + " FROM users WHERE username = %s AND pwd = MD5(%s)"

            cursor.execute(query, (uname, pwd))
            rs = cursor.fetchone()

            user = User.rs(rs)

            return user
        except:
            print("::UserDB getUserByUnameAndPwd error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def getUserByEmailAndPwd(self, conn:pymysql.Connection, email:str, pwd:str):
        cursor = None
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "SELECT " + User.sqlColumns() + " FROM users WHERE email = %s AND pwd = MD5(%s)"
            print("::getUserByEmailAndPwd sql::", query)
            cursor.execute(query, (email, pwd))
            rs = cursor.fetchone()

            user = User.rs(rs)

            return user
        except:
            print("::UserDB getUserByEmailAndPwd error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def getAllUsers(self, conn:pymysql.Connection):
        try:
            userList = list()
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "SELECT " + User.sqlColumns() + " FROM users"

            cursor.execute(query)
            rs = cursor.fetchall()

            for row in rs:
                user = User.rs(row)
                userList.append(user)

            return userList
        except:
            print("::UserDB getAllUsers error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def insertSid(self, conn: pymysql.Connection, sid: str, email: str):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "INSERT INTO sessions (sid, email, created_time) VALUES (%s, %s, now())"
            cursor.execute(query, (sid, email))

            # connection is not autocommit by default. So you must commit to save
            # your changes.

        except:
            print("::UserDB login error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def getUserBySid(self, conn: pymysql.Connection, sid: str):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "SELECT " + User.sqlColumns() + " FROM users WHERE email = ( SELECT email FROM sessions WHERE sid = %s AND is_expired = 0)"

            cursor.execute(query, (sid))
            rs = cursor.fetchone()

            user = User.rs(rs)

            return user
        except:
            print("::UserDB getUserBySid error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def logout(self, conn: pymysql.Connection, email: str, sid:str):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "UPDATE sessions SET is_expired = 1, expired_date = now() WHERE email = %s AND sid = %s AND is_expired = 0"
            cursor.execute(query, (email, sid))

            # connection is not autocommit by default. So you must commit to save
            # your changes.

        except:
            print("::UserDB login error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def insertUser(self, conn: pymysql.Connection, email: str, pwd:str, uname:str, fname:str, type:int, imgUrl:str):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "INSERT INTO users (email, pwd, username, full_name, type, image_url, created_time) VALUES (%s, MD5(%s), %s, %s, %s, %s, now())"
            cursor.execute(query, (email, pwd, uname, fname, type, imgUrl))

            # connection is not autocommit by default. So you must commit to save
            # your changes.

        except:
            print("::UserDB insertUser error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()