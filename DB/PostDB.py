import sys
import pymysql

from DB.UserDB import UserDB
from Models.User import User
from Models.Post import Post

class PostDB:
    def __init__(self):
        x=5

    @classmethod
    def getPostById(self, conn:pymysql.Connection, id:int):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "SELECT " + Post.sqlColumns() + " FROM posts WHERE id = %s"

            cursor.execute(query, (id))
            rs = cursor.fetchone()

            post = Post.rs(rs)
            user = UserDB.getUserById(conn, post.getUserId())
            post.setUser(user)

            return post
        except:
            print("::PostDB getPostById error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def getAllPosts(self, conn: pymysql.Connection):
        try:
            postList = list()
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "SELECT " + Post.sqlColumns() + " FROM posts ORDER BY posts.created_time DESC LIMIT 100"

            cursor.execute(query)
            rs = cursor.fetchall()

            for row in rs:
                post = Post.rs(row)
                user = UserDB.getUserById(conn, post.getUserId())
                post.setUser(user)
                postList.append(post)

            return postList
        except:
            print("::PostDB getAllPosts error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def addNewPost(self, conn: pymysql.Connection, userId: int, message: str, mediaArr:str):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "INSERT INTO posts (user_id, message, media_array, created_time) VALUES (%s, %s, %s, now())"
            cursor.execute(query, (userId, message, mediaArr))

            # connection is not autocommit by default. So you must commit to save
            # your changes.

        except Exception as E:
            print("::PostDB addNewPost error::", E.args[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def editPost(self, conn: pymysql.Connection, postId:int, userId: int, message: str):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "UPDATE posts SET message = %s, modified_time = now() WHERE id = %s AND user_id = %s"
            cursor.execute(query, (message, postId, userId))

            # connection is not autocommit by default. So you must commit to save
            # your changes.

        except Exception as E:
            print("::UserDB editPost error::", E.args[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def isPostLiked(self, conn: pymysql.Connection, postId:int, userId:int):
        try:
            isOk:bool = False
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "SELECT id FROM user_likes WHERE user_id = %s AND post_id = %s"

            cursor.execute(query, (userId, postId))
            rs = cursor.fetchone()

            if rs is not None and len(rs) > 0:
                isOk = True

            return isOk
        except:
            print("::PostDB getPostById error::", sys.exc_info()[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def likePost(self, conn: pymysql.Connection, postId:int, userId: int):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "INSERT INTO user_likes (post_id, user_id, date) VALUES (%s, %s, now())"
            cursor.execute(query, (postId, userId))

            # connection is not autocommit by default. So you must commit to save
            # your changes.

        except Exception as E:
            print("::PostDB addNewPost error::", E.args[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()

    @classmethod
    def unlikePost(self, conn: pymysql.Connection, postId: int, userId: int):
        try:
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            query = "DELETE FROM user_likes WHERE post_id = %s AND user_id = %s"
            cursor.execute(query, (postId, userId))

            # connection is not autocommit by default. So you must commit to save
            # your changes.

        except Exception as E:
            print("::PostDB addNewPost error::", E.args[0])
            raise
        finally:
            print(cursor._last_executed)
            conn.commit()